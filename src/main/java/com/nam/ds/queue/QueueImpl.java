package com.nam.ds.queue;

import com.nam.ds.arraylist.ArrayList;
import com.nam.ds.arraylist.List;

public class QueueImpl<T> implements Queue<T> {

    private final List<T> list = new ArrayList<>();
    private final int maxSize;

    public QueueImpl() {
        maxSize = Integer.MAX_VALUE;
    }

    public QueueImpl(int size) {
        maxSize = size;
    }

    @Override
    public void add(T element) {
        if (list.size() == maxSize) {
            throw new RuntimeException("Queus overflow");
        }
        list.add(element);
    }

    @Override
    public T poll() {
        if (list.isEmpty()) {
            throw new RuntimeException("Queue is empty");
        }
        T val = list.get(0);
        list.remove(0);
        return val;
    }

    @Override
    public int size() {
        return list.size();
    }
}
