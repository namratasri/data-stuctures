package com.nam.ds.stack;

public interface Stack<T> {
    void push(T element);

    T pop();

    T peak();

    int size();
}

