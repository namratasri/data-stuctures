package com.nam.ds.stack;


import com.nam.ds.arraylist.ArrayList;
import com.nam.ds.arraylist.List;
import java.util.EmptyStackException;

public class StackImpl<T> implements Stack<T> {

    private final List<T> list = new ArrayList<>();
    private final int maxSize;

    public StackImpl() {
        maxSize = Integer.MAX_VALUE; // some default size
    }

    public StackImpl(int size) {
        maxSize = size;
    }

    @Override
    public void push(T element) {
        if (list.size() == maxSize) {
            throw new RuntimeException("stack overflow");
        }
        list.add(element);
    }

    @Override
    public T peak() {
        if (list.isEmpty()) {
            //TODO: change this to custom StackUnderflowException
            throw new EmptyStackException();
        }
        return list.get(list.size() - 1);
    }

    @Override
    public T pop() {
        if (list.isEmpty()) {
            //TODO: change this to custom StackUnderflowException
            throw new EmptyStackException();
        }
        int lastIndex = list.size() - 1;
        T lastElement = list.get(lastIndex);
        list.remove(lastIndex);
        return lastElement;
    }

    @Override
    public int size() {
        return list.size();
    }
}
