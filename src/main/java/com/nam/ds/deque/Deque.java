package com.nam.ds.deque;

import com.nam.ds.queue.Queue;
import com.nam.ds.stack.Stack;

public interface Deque<T> extends Queue<T>, Stack<T> {

}
