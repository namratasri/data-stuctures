package com.nam.ds.deque;

import com.nam.ds.arraylist.ArrayList;
import com.nam.ds.arraylist.List;


public class DequeImpl<T> implements Deque<T> {

    private static final String DEQUE_IS_EMPTY = "Deque is empty";
    private final List<T> list = new ArrayList<>();
    private final int maxSize;


    public DequeImpl() {
        maxSize = Integer.MAX_VALUE;
    }

    public DequeImpl(int size) {
        maxSize = size;
    }


    @Override
    public void add(T element) {
        if (list.size() == maxSize) {
            throw new RuntimeException("Deque Overflow");
        }
        list.add(element);

    }

    @Override
    public T poll() {
        if (list.isEmpty()) {
            throw new RuntimeException(DEQUE_IS_EMPTY);
        }
        T firstelmnt = (T) list.get(0);
        list.remove(0);
        return firstelmnt;

    }

    @Override
    public void push(T element) {
        if (list.size() == maxSize) {
            throw new RuntimeException("Deque Overflow");
        }
        list.add(element);
    }

    @Override
    public T pop() {
        if (list.isEmpty()) {
            throw new RuntimeException(DEQUE_IS_EMPTY);
        }
        T lastElmt = (T) list.get(list.size() - 1);
        list.remove(list.size() - 1);
        return lastElmt;
    }

    @Override
    public T peak() {
        if (list.isEmpty()) {
            throw new RuntimeException(DEQUE_IS_EMPTY);
        }
        return (T) list.get(list.size() - 1);
    }

    @Override
    public int size() {
        return list.size();
    }
}
