package com.nam.ds.arraylist;

public class ArrayList<T> implements List<T> {

    private Object[] arr = new Object[5];
    private int size = 0;

    public void add(T element) {
        if (size >= arr.length) {
            copyArr();
        }
        arr[size] = element;
        size++;
    }

    public void copyArr() {
        Object[] newArr = new Object[arr.length + 20];
        if (size >= 0) {
            System.arraycopy(arr, 0, newArr, 0, size);
        }
        arr = newArr;
    }

    public void rightShift(int index) {
        if (size - index >= 0) {
            System.arraycopy(arr, index, arr, index + 1, size - index);
        }
    }

    public void insertAt(int index, T element) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("size of array is : " + size);
        }
        if (size < arr.length) {
            rightShift(index);
        } else {
            copyArr();
            rightShift(index);
        }
        arr[index] = element;
    }

    public T get(int index) {
        if (index >= arr.length) {
            throw new IndexOutOfBoundsException();
        }
        return (T) arr[index];
    }

    public int size() {
        return size;
    }

    public void remove(int index) {
        if (index > (size - 1) || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (size + 1 - index >= 0) {
            System.arraycopy(arr, index + 1, arr, index, size + 1 - index);
        }
        size--;
    }

    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (arr[i] == element) {
                return true;
            }
        }
        return false;
    }

    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (arr[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

}
