package com.nam.ds.arraylist;

public interface List<T> {

    void add(T element);

    void insertAt(int index, T element);

    T get(int index);

    int size();

    void remove(int index);

    boolean contains(T element);

    int indexOf(T element);

    boolean isEmpty();

}
