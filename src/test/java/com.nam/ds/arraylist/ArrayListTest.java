package com.nam.ds.arraylist;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class ArrayListTest {

    @Test
    public void testAdd() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 5).forEachOrdered(i -> {
            list.add(i);
            Assertions.assertEquals(i, list.get(i));
        });
    }

    @Test
    public void testDelete() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 5).forEach(list::add);
        int size = list.size();
        list.remove(size - 1);
        Assertions.assertEquals(size - 1, list.size());
        list.remove(0);
        Assertions.assertEquals(1, list.get(0));
        Assertions.assertEquals(size - 2, list.size());
    }

    @Test
    public void testSize() {
        List<Integer> list = new ArrayList<>();
        Assertions.assertEquals(0, list.size());
        IntStream.rangeClosed(0, 5).forEach(list::add);
        Assertions.assertEquals(6, list.size());
    }

    @Test
    public void testContains() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 5).forEach(list::add);
        Assertions.assertTrue(list.contains(1));
        Assertions.assertFalse(list.contains(-1));
    }

    @Test
    public void testInsertAt() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 3).forEachOrdered(list::add);
        list.insertAt(0, 4);
        Assertions.assertEquals(4, list.get(0));
        IntStream.rangeClosed(1, 4)
                .forEachOrdered(i -> Assertions.assertEquals(i - 1, list.get(i)));
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.insertAt(100, 0));
    }

    @Test
    public void testIndexOf() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 3).forEachOrdered(list::add);
        Assertions.assertEquals(2, list.indexOf(2));
        Assertions.assertEquals(-1, list.indexOf(-1));
    }


    @Test
    public void testGet() {
        List<Integer> list = new ArrayList<>();
        IntStream.rangeClosed(0, 3).forEachOrdered(list::add);
        Assertions.assertEquals(2, list.get(2));
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.get(-1));
    }

}
