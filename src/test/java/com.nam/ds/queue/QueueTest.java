package com.nam.ds.queue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;


public class QueueTest {

    @Test
    public void testPoll() {
        Queue<Integer> queue = new QueueImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(queue::add);
        Assertions.assertEquals(0, queue.poll());
        Assertions.assertEquals(1, queue.poll());
        Assertions.assertEquals(2, queue.poll());
        Assertions.assertThrows(RuntimeException.class, queue::poll);
    }

    @Test
    public void testSize() {
        Queue<Integer> queue = new QueueImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(queue::add);
        Assertions.assertEquals(3, queue.size());
    }

    @Test
    public void testOverflow() {
        Queue<Integer> queue = new QueueImpl<>(3);
        IntStream.rangeClosed(0, 2).forEachOrdered(queue::add);
        Assertions.assertThrows(RuntimeException.class, () -> queue.add(2));
    }
}
