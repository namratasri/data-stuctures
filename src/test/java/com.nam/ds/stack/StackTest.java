package com.nam.ds.stack;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;
import java.util.stream.IntStream;

public class StackTest {
    @Test
    void testStackOverflow() {
        Stack<Integer> stack = new StackImpl<>(3);
        IntStream.rangeClosed(0, 2).forEachOrdered(stack::push);
        Assertions.assertThrows(RuntimeException.class, () -> stack.push(1));
        stack.pop();
        Assertions.assertDoesNotThrow(() -> stack.push(1));
    }

    @Test
    void testPop() {
        var stack = new StackImpl<Integer>();
        IntStream.rangeClosed(0, 2).forEachOrdered(stack::push);
        Assertions.assertEquals(2, stack.pop());
        Assertions.assertEquals(1, stack.pop());
        Assertions.assertEquals(0, stack.pop());
        Assertions.assertThrows(EmptyStackException.class, stack::pop);
    }

    @Test
    void testPeak() {
        Stack<Integer> stack = new StackImpl<>();
        IntStream.rangeClosed(0, 1).forEachOrdered(stack::push);
        Assertions.assertEquals(1, stack.peak());
        stack.pop();
        Assertions.assertEquals(0, stack.peak());
        stack.pop();
        Assertions.assertThrows(EmptyStackException.class, stack::peak);
    }

    @Test
    void testSize() {
        Stack<Integer> stack = new StackImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(stack::push);
        Assertions.assertEquals(3, stack.size());
        stack.pop();
        Assertions.assertEquals(2, stack.size());
        stack.pop();
        stack.pop();
        Assertions.assertEquals(0, stack.size());
    }

}
