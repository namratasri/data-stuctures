package com.nam.ds.deque;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

public class DequeTest {

    @Test
    void testPop() {
        Deque<Integer> deque = new DequeImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(deque::push);
        Assertions.assertEquals(2, deque.pop());
        Assertions.assertEquals(1, deque.pop());
        Assertions.assertEquals(0, deque.pop());
        Assertions.assertThrows(RuntimeException.class, deque::pop);
    }

    @Test
    void testPollPop() {
        Deque<Integer> deque = new DequeImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(deque::push);
        Assertions.assertEquals(2, deque.pop());
        Assertions.assertEquals(0, deque.poll());
    }

    @Test
    void testPeak() {
        Deque<Integer> deque = new DequeImpl<>();
        IntStream.rangeClosed(0, 1).forEachOrdered(deque::push);
        Assertions.assertEquals(1, deque.peak());
        deque.pop();
        Assertions.assertEquals(0, deque.peak());
        deque.poll();
        Assertions.assertThrows(RuntimeException.class, deque::peak);
    }

    @Test
    void testSize() {
        Deque<Integer> deque = new DequeImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(deque::push);
        Assertions.assertEquals(3, deque.size());
        deque.pop();
        Assertions.assertEquals(2, deque.size());
        deque.pop();
        deque.poll();
        Assertions.assertEquals(0, deque.size());
    }


    @Test
    public void testPoll() {
        Deque<Integer> deque = new DequeImpl<>();
        IntStream.rangeClosed(0, 2).forEachOrdered(deque::add);
        Assertions.assertEquals(0, deque.poll());
        Assertions.assertEquals(1, deque.poll());
        Assertions.assertEquals(2, deque.poll());
        Assertions.assertThrows(RuntimeException.class, deque::poll);
    }
}
